@extends('bpanel4-public.layouts.regular-page')

@section('title')
    Pago completado
@endsection

@section('content')
    <div class="regular-page-container">
        <div class="d-flex justify-content-center">
            <div class="d-flex flex-column justify-content-center align-items-center"
                 style="min-height: 600px; max-width: 1000px">
                <h1 class="mb-3 text-success"><i class="far fa-thumbs-up"></i></h1>
                <h2>El pago se realizó correctamente</h2>
                <p class="text-center text-muted">Revise su correo o su área de cliente para
                ver los detalles de su pedido.</p>
                <p class="text-center text-muted">¡Muchas gracias!</p>
            </div>
        </div>
    </div>

@endsection
