@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Error en el pago de su pedido</h2>
    <p>{{ $order->getClient()->getName() }}, ocurrió un error con el pago de su pedido {{ \Bittacora\Laravel\Redsys\Services\OrderNumberFormatter::formatOrderNumber($order->getId()) }}.</p>
    <p>Por favor, vuelva a intentar realizar su pedido o póngase en contacto con notrosos.</p>
@endsection
