@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Error en el pago del pedido {{ $order->getId() }}</h2>
    <p>Ocurrió un error con el pago del pedido {{ \Bittacora\Laravel\Redsys\Services\OrderNumberFormatter::formatOrderNumber($order->getId()) }}.</p>
    <p>Se ha enviado un email al cliente para que vuelva a intentar realizar el pedido o se ponga en contacto con {{ config('app.name') }}.</p>
@endsection
