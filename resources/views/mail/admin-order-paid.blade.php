@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Se ha recibido el pago por bizum del pedido {{ $order->getId() }}</h2>
    <p>Se ha recibido la confirmación del pago por bizum del pedido {{ \Bittacora\Laravel\Redsys\Services\OrderNumberFormatter::formatOrderNumber($order->getId()) }}.</p>
@endsection
