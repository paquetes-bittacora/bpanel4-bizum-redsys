@extends('bpanel4-public.layouts.regular-page')

@section('title')
    Error en el pago por bizum
@endsection

@section('content')
    <div class="regular-page-container">
        <div class="d-flex justify-content-center">
            <div class="d-flex flex-column justify-content-center align-items-center"
                 style="min-height: 600px; max-width: 1000px">
                <h1 class="mb-3 text-danger"><i class=" far fa-exclamation-triangle"></i></h1>
                <h2>Ocurrió un error al procesar el pago</h2>
                <p class="text-center text-muted">El error se produjo en la pasarela de pago del banco, y puede deberse a distintas causas: no ha
                   introducido
                   el código de verificación que puede haber recibido por SMS o la aplicación de su banco, problemas con
                   su tarjeta,
                   límites diarios alcanzados, etc.</p>
                <p class="text-center text-muted">Por favor, vuela a realizar su pedido y compruebe que no se está produciendo ninguno de los errores
                   mencionados,
                   y si el problema persiste, póngase en contacto con nosotros.</p>
            </div>
        </div>
    </div>

@endsection
