<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Actions;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaymentErrorAdminMail;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaymentErrorClientMail;

final class MarkOrderAsPaymentError extends MarkOrderAs
{
    protected const ORDER_STATUS = OrderStatus::PAYMENT_ERROR;

    protected function setOrderStatus(Order $order): void {
        parent::setOrderStatus($order);
        $this->restoreClientCart($order);
    }

    protected function getClientMailable(): OrderPaymentErrorClientMail
    {
        return new OrderPaymentErrorClientMail($this->getOrder());
    }

    protected function getAdminMailable(): OrderPaymentErrorAdminMail
    {
        return new OrderPaymentErrorAdminMail($this->getOrder());
    }

    private function restoreClientCart(Order $order): void
    {
        $clientId = $order->getClient()->getClientId();
        $dateThreshold = new \DateTime('10 minutes ago');
        $cart = Cart::where('client_id', $clientId)
            ->onlyTrashed()
            ->where('deleted_at', '>', $dateThreshold->format('Y-m-d H:i:s'))
            ->orderBy('id', 'DESC')->first();

        if ($cart === null) {
            return;
        }

        $cart->restore();
    }
}
