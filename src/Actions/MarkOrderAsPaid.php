<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Actions;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaidAdminMail;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaidClientMail;
use Illuminate\Contracts\Mail\Mailable;

final class MarkOrderAsPaid extends MarkOrderAs
{
    protected const ORDER_STATUS = OrderStatus::PAYMENT_COMPLETED;

    protected function getClientMailable(): Mailable
    {
        return new OrderPaidClientMail($this->getOrder());
    }

    protected function getAdminMailable(): Mailable
    {
        return new OrderPaidAdminMail($this->getOrder());
    }
}
