<?php

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Actions;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Repositories\OrderRepository;
use Bittacora\Laravel\Redsys\Dtos\TpvNotificationResult;
use Bittacora\Laravel\Redsys\Services\OrderNumberFormatter;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Contracts\Mail\Mailer;

abstract class MarkOrderAs
{
    /** @var OrderStatus */
    protected const ORDER_STATUS = OrderStatus::CREATED;
    protected ?Order $order = null;

    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly Mailer $mailer,
        private readonly Repository $config,
    ) {
    }

    public function handle(TpvNotificationResult $notificationResult): void
    {
        $order = $this->getOrder($notificationResult);

        $this->setOrderStatus($order);
        $this->sendClientEmail($this->getClientMailable());
        $this->sendAdminEmail($this->getAdminMailable());
    }

    protected function getOrderId(TpvNotificationResult $notificationResult): int
    {
        return OrderNumberFormatter::fromTpvFormat($notificationResult->dsOrder);
    }

    protected function getOrder(?TpvNotificationResult $notificationResult = null): Order
    {
        if (null === $this->order) {
            $this->order = $this->orderRepository->getById($this->getOrderId($notificationResult));
        }
        return $this->order;
    }

    protected function setOrderStatus(Order $order): void
    {
        $order->setStatus(OrderStatusModel::whereId(static::ORDER_STATUS->value)->firstOrFail());
        $order->save();
    }

    protected function sendClientEmail(Mailable $mailable): void
    {
        $this->mailer->to($this->getOrder()->getClient()->getEmail())->send($mailable);
    }

    protected function sendAdminEmail(Mailable $mailable): void
    {
        $adminEmail = $this->config->get('bpanel4.admin_email');

        if ($this->config->has('orders.orders_admin_email')) {
            $adminEmail = $this->config->get('orders.orders_admin_email');
        }

        $this->mailer->to($adminEmail)->send($mailable);
    }

    abstract protected function getClientMailable(): Mailable;
    abstract protected function getAdminMailable(): Mailable;
}
