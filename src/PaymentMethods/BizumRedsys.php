<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\PaymentMethods;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Repositories\OrderRepository;
use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

final class BizumRedsys implements PaymentMethod
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly Redirector $redirector,
    ) {
    }

    public function processPayment(OrderDetailsDto $orderDetails): RedirectResponse
    {
        $order = $this->orderRepository->getById($orderDetails->orderId);
        $order->setStatus(OrderStatusModel::whereId(OrderStatus::PAYMENT_PENDING->value)->firstOrFail());

        return $this->redirector->signedRoute(
            'bpanel4-bizum-redsys.show-form',
            ['order' => $order->getId()]
        );
    }

    public function getUserInstructions(): ?string
    {
        return null;
    }

    public function getName(): string
    {
        return 'Pago con bizum';
    }
}
