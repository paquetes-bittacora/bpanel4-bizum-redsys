<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Mail;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Mail\Mailable;

final class OrderPaymentErrorClientMail extends Mailable
{
    public function __construct(private readonly Order $order)
    {
    }

    public function build(): OrderPaymentErrorClientMail
    {
        return $this->subject('Error en el pago de su pedido')
            ->view('bpanel4-bizum-redsys::mail.client-order-payment-error', [
                'client' => $this->order->getClient(),
                'order' => $this->order,
            ]);
    }
}
