<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Mail;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Mail\Mailable;

final class OrderPaidClientMail extends Mailable
{
    public function __construct(private readonly Order $order)
    {
    }

    public function build(): OrderPaidClientMail
    {
        return $this->subject('Pago completado')
            ->view('bpanel4-bizum-redsys::mail.client-order-paid', [
                'client' => $this->order->getClient(),
                'order' => $this->order,
            ]);
    }
}
