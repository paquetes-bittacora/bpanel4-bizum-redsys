<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys;

use Bittacora\Bpanel4\Payment\BizumRedsys\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class Bpanel4BizumRedsysServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-bizum-redsys';

    public function boot(): void
    {
        $this->commands(InstallCommand::class);

        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
    }
}
