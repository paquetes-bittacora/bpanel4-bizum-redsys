<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Payment\Redsys\Services\BpanelRedsysNotificationService;
use Bittacora\Laravel\Redsys\Services\OrderNumberFormatter;
use Bittacora\Laravel\Redsys\Services\TpvFormService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Sermepa\Tpv\TpvException;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class BizumRedsysPaymentController extends Controller
{
    public function __construct(private readonly Factory $view)
    {
    }

    public function showForm(Request $request, Order $order, TpvFormService $tpvFormService): View
    {
        if (!$request->hasValidSignature()) {
            throw new HttpException(401, 'El enlace no es válido');
        }

        $description = implode(', ', $order->getProducts()->pluck('name')->toArray());
        $form = $tpvFormService->renderTpvForm(
            $order->getOrderTotal(),
            OrderNumberFormatter::formatOrderNumber($order->getId()),
            $description,
            'z'
        );
        return $this->view->make('bpanel4-bizum-redsys::show-form', ['form' => $form]);
    }

    public function showUrlKo(): View
    {
        return $this->view->make('bpanel4-bizum-redsys::url-ko');
    }

    public function showUrlOk(): View
    {
        return $this->view->make('bpanel4-bizum-redsys::url-ok');
    }

    /**
     * @throws TpvException
     */
    public function processTpvNotification(Request $request, BpanelRedsysNotificationService $notificationService): void
    {
        $notificationService->processNotification($request);
    }
}
