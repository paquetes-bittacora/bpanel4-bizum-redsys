<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Services;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\UrlGenerator;

final class ConfigFileUpdater
{
    private const PUBLISHED_CONFIG_FILE_PATH = 'config/redsys.php';

    public function __construct(
        Application $application,
        private readonly UrlGenerator $urlGenerator,
        private ?string $basePath = '',
    ) {
        $this->basePath = '' !== $this->basePath ? $basePath : $application->basePath();
    }

    public function setUrlOkAndKo(): void
    {
        $configFilePath = $this->basePath . DIRECTORY_SEPARATOR . self::PUBLISHED_CONFIG_FILE_PATH;
        $originalFileContents = file_get_contents($configFilePath);

        $content = str_replace(
            ["env('REDSYS_URL_OK','')", "env('REDSYS_URL_KO','')"],
            [
                "'" . $this->urlGenerator->route('bpanel4-bizum-redsys.url-ok') . "'",
                "'" . $this->urlGenerator->route('bpanel4-bizum-redsys.url-ko') . "'",
            ],
            $originalFileContents
        );

        file_put_contents($configFilePath, $content);
    }

    public function setNotificationUrl(): void
    {
        $configFilePath = $this->basePath . DIRECTORY_SEPARATOR . self::PUBLISHED_CONFIG_FILE_PATH;
        $originalFileContents = file_get_contents($configFilePath);

        $content = str_replace(
            "env('REDSYS_URL_NOTIFICATION','')",
            "'" . $this->urlGenerator->route('bpanel4-bizum-redsys.url-notification') . "'",
            $originalFileContents
        );

        file_put_contents($configFilePath, $content);
    }
}
