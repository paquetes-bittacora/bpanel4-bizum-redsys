<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\BizumRedsys\Services;

use Bittacora\Bpanel4\Payment\Redsys\Actions\MarkOrderAsPaid;
use Bittacora\Bpanel4\Payment\Redsys\Actions\MarkOrderAsPaymentError;
use Bittacora\Laravel\Redsys\Services\TpvNotificationService;
use Illuminate\Http\Request;
use Sermepa\Tpv\TpvException;

final class BpanelRedsysNotificationService
{
    public function __construct(
        private readonly TpvNotificationService $tpvNotificationService,
        private readonly MarkOrderAsPaid $markOrderAsPaid,
        private readonly MarkOrderAsPaymentError $markOrderAsPaymentError,
    ) {
    }

    /**
     * @throws TpvException
     */
    public function processNotification(Request $request): void
    {
        $result = $this->tpvNotificationService->processNotification($request);
        if ($result->success) {
            $this->markOrderAsPaid->handle($result);
            return;
        }
        $this->markOrderAsPaymentError->handle($result);
    }
}
