<?php

declare(strict_types=1);


use App\Http\Middleware\VerifyCsrfToken;
use Bittacora\Bpanel4\Payment\BizumRedsys\Http\Controllers\BizumRedsysPaymentController;
use Illuminate\Support\Facades\Route;

Route::prefix('/pago-bizum')->name('bpanel4-bizum-redsys.')->middleware(['web'])->group(function () {
    Route::get('/pedido/{order}', [BizumRedsysPaymentController::class, 'showForm'])->name('show-form');
    Route::get('/error-pago', [BizumRedsysPaymentController::class, 'showUrlKo'])->name('url-ko');
    Route::get('/pago-ok', [BizumRedsysPaymentController::class, 'showUrlOk'])->name('url-ok');
    Route::post('/notificacion-tpv', [BizumRedsysPaymentController::class, 'processTpvNotification'])
        ->name('url-notification')->withoutMiddleware([VerifyCsrfToken::class]);
});
